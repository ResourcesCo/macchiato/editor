export class FileGroup extends HTMLElement {
  textEn = {
    addFile: 'Add File',
  }

  textEs = {
    addFile: 'Añadir archivo',
  }

  constructor() {
    super()
    this.language = navigator.language
    this.attachShadow({mode: 'open'})
    this.headerEl = document.createElement('div')
    this.headerEl.classList.add('header')
    this.contentEl = document.createElement('div')
    this.contentEl.classList.add('content')
    this.shadowRoot.appendChild(this.headerEl)
    this.shadowRoot.appendChild(this.contentEl)
    const bGroup = document.createElement(
      'm-forms-button-group'
    )
    bGroup.addPrimary(this.text.addFile, () => {
      this.addFile()
      const btn = bGroup.primary
      if (btn.scrollIntoViewIfNeeded) {
        btn.scrollIntoViewIfNeeded()
      } else {
        btn.scrollIntoView()
      }
    })
    this.shadowRoot.appendChild(bGroup)
    this.contentEl.addEventListener(
      'click-add-above',
      e => {
        const el = document.createElement(
          'm-editor-file-view'
        )
        e.target.insertAdjacentElement(
          'beforebegin', el
        )
      },
    )
    this.contentEl.addEventListener(
      'click-add-below',
      e => {
        const el = document.createElement(
          'm-editor-file-view'
        )
        e.target.insertAdjacentElement(
          'afterend', el
        )
      },
    )
  }

  connectedCallback() {
    const style = document.createElement('style')
    style.textContent = `
      :host {
        display: flex;
        flex-direction: column;
        align-items: stretch;
      }
      div.header {
        display: flex;
        flex-direction: row;
      }
      div.files {
        display: flex;
        flex-direction: column;
        flex-grow: 1;
        overflow-y: auto;
      }
    `
    this.shadowRoot.appendChild(style)
    if (this.contentEl.childNodes.length === 0) {
      this.addFile()
    }
  }

  addFile({name, data, collapsed} = {}) {
    const el = document.createElement('m-editor-file-view')
    if (name !== undefined) {
      el.name = name
    }
    if (data !== undefined) {
      el.data = data
    }
    if (collapsed !== undefined) {
      el.collapsed = collapsed
    }
    this.contentEl.appendChild(el)
    return el
  }

  get language() {
    return this._language
  }

  set language(language) {
    this._language = language
    this.text = this.langEs ? this.textEs : this.textEn
  }

  get langEs() {
    return /^es\b/.test(this.language)
  }

  get files() {
    return [...this.contentEl.children]
  }
}